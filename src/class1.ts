import * as Promise from 'bluebird';
import { Class2 } from "./class2";

export class Class1 {
  static myFunc(): Promise<boolean> {
    return Class2.myFunc();
  }
}
